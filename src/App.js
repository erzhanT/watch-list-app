import React, {Component} from 'react';
import './App.css';
import RadioButton from "./components/RadioButton/RadioButton";


class App extends Component {


    render() {
        return (
            <div className="App">
              <RadioButton />
            </div>
        );
    }
}

export default App;