import React, {Component} from 'react';

class Movie extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.movies !== this.props.movies;
    }

    render() {
        console.log('render')
        return (
            this.props.movies.map((movie, index) => {
                return (
                    <div
                        key={index}>
                        <input
                            type="text"
                            value={movie.title}
                            onChange={(e) => this.props.changeMovie(e, movie.id)}
                        />
                        <button onClick={() => this.props.removeMovie(movie.id)}>&times;</button>
                    </div>
                )
            })
        );
    }
}

export default Movie;