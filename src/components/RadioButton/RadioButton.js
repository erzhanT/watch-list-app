import React, {useState} from 'react';
import FirstTask from "../FirstTask/FirstTask";
import SecondTask from "../SecondTask/SecondTask";

const RadioButton = () => {

    const [mode, setMode] = useState('first');

    const onRadioChange = e => {
        setMode(e.target.value);
    }

    return (
        <div>
            <p>
                <input
                    type="radio"
                    name="options"
                    value="first"
                    onChange={onRadioChange}
                    checked={mode === 'first'}
                /> Список фильмов
            </p>
            <p>
                <input
                    type="radio"
                    name="options"
                    value="second"
                    onChange={onRadioChange}
                    checked={mode === 'second'}
                /> Анекдоты с Чаком Норрисом
            </p>
            <div>
                {mode === 'first' && (
                    <FirstTask/>
                )}
                {mode === 'second' && (
                    <SecondTask/>
                )}
            </div>
        </div>
    );
};

export default RadioButton;