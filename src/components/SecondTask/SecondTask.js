import React, {useState, useEffect} from 'react';
import './SecondTask.css';
import axios from 'axios';

const SecondTask = () => {

    const [joke, setJoke] = useState([]);

    const url = 'https://api.chucknorris.io/jokes/random';

    useEffect(() => {
        getJokes()
    }, [])

    const getJokes = async () => {
        try {
            const response = await axios.get(url);
            setJoke(response.data.value);
        } catch (e) {
            console.log(e);
        }
    };

    return (
        <div className="second-task">
            <h3>{joke}</h3>
        </div>
    )
};

export default SecondTask;