import React, {Component} from 'react';
import {nanoid} from 'nanoid';
import './FirstTask.css';
import Movie from "../Movie/Movie";

class FirstTask extends Component {

    state = {
        newMovieTitle: '',
        movies: []
    };

    addMovie = (e) => {
        e.preventDefault();
        const movies = [...this.state.movies];
        const movie = {id: nanoid(), title: this.state.newMovieTitle};
        movies.push(movie);
        this.setState({movies: movies, newMovieTitle: ''});
    };

    onChangeHandler = (e) => {
        this.setState({newMovieTitle: e.target.value})
    }

    changeMovie = (e, id) => {
        const movies = [...this.state.movies];
        const index = movies.findIndex(i => i.id === id);
        movies[index].title = e.target.value;
        this.setState({movies});
    };


    removeMovie = id => {
        const movies = [...this.state.movies];
        const index = movies.findIndex(i => i.id === id);
        movies.splice(index, 1)
        this.setState({movies})
    };


    render() {
        const movies = this.state.movies;
        return (
            <div className="first-task">
                <h3>First Task</h3>
                <div>
                    <form onSubmit={this.addMovie}>
                        <input
                            type="text"
                            key={movies.id}
                            value={this.state.newMovieTitle}
                            onChange={(e) => this.onChangeHandler(e)}
                        />
                        <button>Add</button>
                    </form>
                    <Movie movies={movies} changeMovie={this.changeMovie} removeMovie={this.removeMovie}/>
                </div>
            </div>
        );
    }
}

export default FirstTask;